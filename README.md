

**Dynamic Carousel**
----------------

> *By [Efthaqur Alam](http://efthaqur.mynetwall.info).*

----------

> **This project was inspired by LAB Exam 4**
> **Published Here Practicing Purpose**

----------

> This is a Dynamic Carousel which slideshow based on user input.
> *As the frontend was designed using bootstrap and user input taken by PHP.*
> Based on very simple bootstrap structure, and simplified to learn.
> Planing to add some animation using jQueryUI, still not included.

----------

> Md. Eftequarul Alam
> SEID 137008 Batch - B33
> WEB App using PHP


----------


