<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Upload Files Here!</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div class="container">
    <div class="row jumbotron">
        <div class="col-md-8">
            <h1>Dynamic Carousel</h1>
            <p>Use this page to upload your images, See the Bootstrap Carousel!!</p>
        </div>
        <div class="col-md-4">
            <img src="assets/image/display_pic.jpg" alt="Display Picture" class="img-circle size">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h4><span class="glyphicon glyphicon-ok purple-mark"> </span> Step 1: Upload your slides or Photo! </h4>
                    <p>Here you will find a button to open upload panel! Click in button and upload your slides or photos with description!</p>

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">
                        Click Here to Upload!
                    </button>

                    <h4><span class="glyphicon glyphicon-ok purple-mark"> </span> Step 2: Preview! </h4>
                    <p>Confirm That You have chosen your photo's or slides perfectly and also in perfect order!!</p>

                    <h4><span class="glyphicon glyphicon-ok purple-mark"> </span> Step 3: See Carousel! </h4>
                    <p>Click on Upload Button and see Slide show pictures you have uploaded!</p>

                </div>
            </div>



        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <form action="carousel.php" method="POST" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="slide1title">First Image Title</label>
                        <input type="text" class="form-control" name="slide1title" id="">
                        <label for="slide1notes">First Image Description</label>
                        <input type="text" class="form-control" name="slide1notes" id="">
                        <label for="slide1">Firist Image</label>
                        <input type="file" class="form-control" name="slide1" id="" >
                    </div>

                    <div class="form-group">
                        <label for="slide2title">Second Image Title</label>
                        <input type="text" class="form-control" name="slide2title" id="">
                        <label for="slide2notes">Second Image Description</label>
                        <input type="text" class="form-control" name="slide2notes" id="">
                        <label for="slide2">Second Image</label>
                        <input type="file" class="form-control" name="slide2" id="" >
                    </div>

                    <div class="form-group">
                        <label for="slide3title">Third Image Title</label>
                        <input type="text" class="form-control" name="slide3title" id="">
                        <label for="slide3notes">Third Image Description</label>
                        <input type="text" class="form-control" name="slide3notes" id="">
                        <label for="slide3">Third Image</label>
                        <input type="file" class="form-control" name="slide3" id="" >
                    </div>

                    <input type="submit" class="btn btn-info" value="Upload!">

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>


</div>

<footer class="footer">
    <p class="text-muted text-center">Eftequarul Alam SEID 137008 BITM Batch 33</p>
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>
