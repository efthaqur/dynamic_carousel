
<?php


        $slide1="slide1.jpg";
        $slide2="slide2.jpg";
        $slide3="slide3.jpg";

        $tempSlide1=$_FILES['slide1']['tmp_name'];
        $tempSlide2=$_FILES['slide2']['tmp_name'];
        $tempSlide3=$_FILES['slide3']['tmp_name'];

        move_uploaded_file($tempSlide1, 'img/'.$slide1);
        move_uploaded_file($tempSlide2, 'img/'.$slide2);
        move_uploaded_file($tempSlide3, 'img/'.$slide3);






$slide1title = $_POST['slide1title'];
$slide1notes = $_POST['slide1notes'];

$slide2title = $_POST['slide2title'];
$slide2notes = $_POST['slide2notes'];

$slide3title = $_POST['slide3title'];
$slide3notes = $_POST['slide3notes'];

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Upload Files Here!</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div class="container">
    <div class="row jumbotron">
        <div class="col-md-8">
            <h1>Dynamic Carousel</h1>
            <p>Use this page to upload your images, See the Bootstrap Carousel!!</p>
        </div>
        <div class="col-md-4">
            <img src="assets/image/display_pic.jpg" alt="Display Picture" class="img-circle size">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="img/slide1.jpg" alt="slide 1">
                        <div class="carousel-caption">
                            <h3><?php echo $slide1title?></h3>
                            <p><?php echo $slide1notes ?></p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="img/slide2.jpg" alt="slide 2">
                        <div class="carousel-caption">
                            <h3><?php echo $slide2title?></h3>
                            <p><?php echo $slide2notes ?></p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="img/slide3.jpg" alt="slide 2">
                        <div class="carousel-caption">
                            <h3><?php echo $slide3title?></h3>
                            <p><?php echo $slide3notes ?></p>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        <buttnon class="btn btn-info" onClick="window.location='index.php'">Back to Previous Page</buttnon>
        </div>
    </div>
</div>
<footer class="footer">
    <p class="text-muted text-center">Eftequarul Alam SEID 137008 BITM Batch 33</p>

</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>
